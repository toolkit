 /* toolkit. provides different widgets, implements and modules for 
 * building audio based applications in webbrowsers.
 * 
 * Invented 2013 by Markus Schmidt <schmidt@boomshop.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this program; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, 
 * Boston, MA  02110-1301  USA
 */
 
LevelMeter = $class({
    // LevelMeter is a fully functional display of numerical values. They are
    // enhanced MeterBases containing a clip LED, a peak pin with value label
    // and hold markers. LevelMeters have some automatically triggered
    // functionality like falling and resetting all kinds of values after a
    // time. All additional elements can be set automatically as soon as the
    // value rises above them.
    _class: "LevelMeter",
    Extends: MeterBase,
    options: {
        clip:         false,   // state of the clipping LED
        falling:      0,       // if falling is active set a step size per frame
        falling_fps:  24,      // frames per second for falling
        falling_init: 2,       // frames of the initial falling (avoid
                               // flickering while using external and internal
                               // falling)
        peak:         false,   // peak value; false = value
        top:          false,   // top hold value; false = value
        bottom:       false,   // bottom hold value (if we have a base other
                               // than min)
        hold_size:    1,       // amount of segments for top hold
        show_peak:    false,   // show the peak marker
        show_clip:    false,   // show clipping LED
        show_hold:    false,   // show peak hold LEDs
        clipping:     0,       // if auto_clip is active, value when clipping
                               // appears
        auto_clip:    false,   // set to a number when clipping appears or to
                               // false if disabled
                               //     -1:    infinite positioning
                               //     n:     in milliseconds to auto-reset
                               //     false: no auto peak
        auto_peak:    false,   // if the peak automatically follows the program:
                               //     -1:    infinite positioning
                               //     n:     in milliseconds to auto-reset
                               //     false: no auto peak
        peak_label:   false,   // if the label automatically shows the max peak
                               //     -1:    infinite display
                               //     n:     in milliseconds to auto-reset
                               //     false: no peak label
        auto_hold:    false,   // if the hold automatically follows the program:
                               //     -1:    infinite positioning
                               //     n:     in milliseconds to auto-reset
                               //     false: no auto hold
        format_peak: function (value) { return value.toFixed(2); },
        clip_options: {}       // add options for the clipping LED
    },
    
    initialize: function (options) {
        MeterBase.prototype.initialize.call(this, options, true);
        this.element.classList.add("toolkit-level-meter");
        
        this.state = new State(Object.append({
            "class": "toolkit-clip"
        }, this.options.clip_options));
        this._clip = this.state.element;
        
        if (this.options.layout == _TOOLKIT_TOP
        || this.options.layout == _TOOLKIT_BOTTOM) {
            this.state.element.inject(this._bar, "after");
        } else {
            this.state.element.inject(this._scale, "before");
        }
        this._peak       = toolkit.element("div","toolkit-peak");
        this._peak_label = toolkit.element("div","toolkit-peak-label");
        this._mask3      = toolkit.element("div","toolkit-mask","toolkit-mask3");
        this._mask4      = toolkit.element("div","toolkit-mask","toolkit-mask4");
        this._peak.appendChild(this._peak_label);
        this.element.appendChild(this._peak);
        this._bar.appendChild(this._mask3);
        this._bar.appendChild(this._mask4);

        
        this._falling = this.options.value;
        if (this.options.peak === false)
            this.options.peak = this.options.value;
        if (this.options.top === false)
            this.options.top = this.options.value;
        if (this.options.bottom === false)
            this.options.bottom = this.options.value;
        toolkit.set_styles(this._mask3, {
            position: "absolute",
            zIndex:  "1000"
        });
        toolkit.set_styles(this._mask4, {
            position: "absolute",
            zIndex:  "1000"
        });
        if (this.options.layout == _TOOLKIT_LEFT
        || this.options.layout == _TOOLKIT_RIGHT) {
            if (this.options.reverse) {
                toolkit.set_styles(this._mask3, {
                    width:  "100%",
                    height: "0px",
                    bottom: "0px"
                });
                toolkit.set_styles(this._mask4, {
                    width:  "100%",
                    height: "0px",
                    top: "0px"
                });
            } else {
                toolkit.set_styles(this._mask3, {
                    width:  "100%",
                    height: "0px",
                    top: "0px"
                });
                toolkit.set_styles(this._mask4, {
                    width:  "100%",
                    height: "0px",
                    bottom:    "0px"
                });
            }
        } else {
            if (this.options.reverse) {
                toolkit.set_styles(this._mask3, {
                    height: "100%",
                    width:  "0px",
                    left:   "0px"
                });
                toolkit.set_styles(this._mask4, {
                    height: "100%",
                    width:  "0px",
                    right:  "0px"
                });
            } else {
                toolkit.set_styles(this._mask3, {
                    height: "100%",
                    width:  "0px",
                    right:  "0px"
                });
                toolkit.set_styles(this._mask4, {
                    height: "100%",
                    width:  "0px",
                    left:   "0px"
                });
            }
        }
        
        this.set("show_peak", this.options.show_peak);
        this.set("show_clip", this.options.show_clip);
        this.set("show_hold", this.options.show_hold);
        this.set("clip", this.options.clip);
        this.reset_label = this.reset_label.bind(this);
        this.redraw();
        this.initialized();
    },
    
    redraw: function () {
        switch (this.options.layout) {
            case _TOOLKIT_LEFT:
            case _TOOLKIT_RIGHT:
                this.__margin = toolkit.css_space(this._bar,
                    "margin", "border", "padding"
                ).top + this._bar.getPosition(this.element).y;
                var m = (this.options.show_clip ? toolkit.outer_height(this._clip, true) : 0);
                this._scale.style["top"] = m + "px";
                break;
            case _TOOLKIT_TOP:
            case _TOOLKIT_BOTTOM:
                this.__margin = toolkit.css_space(this._bar,
                    "margin", "border", "padding"
                ).left + this._bar.getPosition(this.element).x;
                break;
        }
        this.set("peak", this.options.peak);
        MeterBase.prototype.redraw.call(this);
    },
    destroy: function () {
        this.state.destroy();
        this._peak.destroy();
        this._peak_label.destroy();
        this._mask3.destroy();
        this._mask4.destroy();
        MeterBase.prototype.destroy.call(this);
    },
    reset_peak: function () {
        this.set("peak", this.options.value);
        this.fire_event("resetpeak", this);
    },
    reset_label: function () {
         this.__lto = false;
        this.set("label", this.options.value);
        this.fire_event("resetlabel", this);
    },
    reset_clip: function () {
        this.__cto = false;
        this.set("clip", false);
        this.fire_event("resetclip", this);
    },
    reset_top: function () {
        this.set("top", this.options.value);
        this.fire_event("resettop", this);
    },
    reset_bottom: function () {
        this.set("bottom", this.options.value);
        this.fire_event("resetbottom", this);
    },
    reset_all: function () {
        this.reset_label();
        this.reset_peak();
        this.reset_clip();
        this.reset_top();
        this.reset_bottom();
    },
    
    draw_meter: function (value) {
        var _c = true;
        if (this.options.falling) {
            if (this.options.value > this._falling
            && this.options.value > this.options.base
            || this.options.value < this._falling
            && this.options.value < this.options.base) {
                this._falling = this.options.value;
                this.__falling = false;
            } else if (typeof value == "undefined") {
                if (this._falling > this.options.base)
                    this._falling -= Math.min(Math.abs(
                        this._falling - this.options.base
                    ), Math.abs(this.options.falling));
                else if (this._falling < this.options.base)
                    this._falling += Math.min(Math.abs(
                        this.options.base - this._falling
                    ), Math.abs(this.options.falling));
                _c = false;
                this.__falling = true;
            }
            this.options.value = this._falling;
            this._falling_timeout();
        }
        if (this.options.peak_label !== false
        && this.options.value > this.options.label
        && this.options.value > this.options.base
        || this.options.value < this.options.label
        && this.options.value < this.options.base) {
            this.set("label", this.options.value);
        }
        if (this.options.auto_peak !== false
        && this.options.value > this.options.peak
        && this.options.value > this.options.base
        || this.options.value < this.options.peak
        && this.options.value < this.options.base) {
            this.set("peak", this.options.value);
        }
        if (this.options.auto_clip !== false
        && _c
        && this.options.value > this.options.clipping
        && !this.__based) {
            this.set("clip", true);
        }
        if (this.options.auto_hold !== false
        && this.options.show_hold
        && this.options.value > this.options.top) {
            this.set("top", this.options.value, true);
        }
        if (this.options.auto_hold !== false
        && this.options.show_hold
        && this.options.value < this.options.bottom
        && this.__based) {
            this.set("bottom", this.options.value, true);
        }

        var vert = !!this._vert();
        
        if (!this.options.show_hold) {
            MeterBase.prototype.draw_meter.call(this);
            if (!this.__tres) {
                this.__tres = true;
                this._mask3.style[vert ? "height" : "width"] = 0;
                this._mask4.style[vert ? "height" : "width"] = 0;
            }
        } else {
            this.__tres = false;
            
            var m1 = this._mask1.style;
            var m2 = this._mask2.style;
            var m3 = this._mask3.style;
            var m4 = this._mask4.style;
           
            // shorten things
            var r         = this.options.reverse;
            var base      = this.options.base;
            var size      = this.options.basis;
            var value     = this.options.value;
            var top       = this.options.top;
            var bottom    = this.options.bottom;
            var hold_size = this.options.hold_size;
            var segment   = this.options.segment;
            
            var _top      = +this._val2seg(Math.max(top, base));
            var top_val   = +this._val2seg(Math.max(value, base));
            var top_top   = Math.max(top_val, _top);
            var top_bot   = top_top - segment * hold_size;
            var top_size  = Math.max(0, _top - top_val - segment * hold_size);
            
            m1[vert ? "height" : "width"] = Math.max(0, size - top_top) + "px";
            m3[vert ? (r ? "bottom" : "top")
                            : (r ? "left" : "right")] = (size - top_bot) + "px";
            m3[vert ? "height" : "width"] = top_size + "px";
            
            if (this.__based) {
                var _bot     = +this._val2seg(Math.min(bottom, base));
                var bot_val  = +this._val2seg(Math.min(value, base));
                var bot_bot  = Math.min(bot_val, _bot);
                var bot_top  = bot_bot + segment * hold_size;
                var bot_size = Math.max(0, bot_val - bot_top);
                
                m2[vert ? "height" : "width"] = Math.max(0, bot_bot) + "px";
                m4[vert ? (r ? "top" : "bottom")
                        : (r ? "right" : "left")] = bot_top + "px";
                m4[vert ? "height" : "width"] = bot_size + "px";
            }
            this.fire_event("drawmeter", this);
        }
    },
    
    draw_peak: function () {
        var n = this._peak_label;
        var v = this.options.format_peak(this.options.peak);
        if (n.firstChild) {
            n.firstChild.nodeValue = v;
        } else n.appendChild(document.createTextNode(v));
        if (this.options.peak > this.options.min
        && this.options.peak < this.options.max
        && this.options.show_peak) {
            this._peak.style["display"] = "block";
            var pos = 0;
            switch (this.options.layout) {
                case _TOOLKIT_LEFT:
                case _TOOLKIT_RIGHT:
                    pos = this.options.basis
                        - this.val2px(this.options.peak)
                        + this.__margin;
                    pos = Math.max(this.__margin, pos);
                    pos = Math.min(this.options.basis + this.__margin, pos);
                    this._peak.style["top"] = pos + "px";
                    break;
                case _TOOLKIT_TOP:
                case _TOOLKIT_BOTTOM:
                    pos = this.val2px(this.options.peak)
                        + this.__margin;
                    pos = Math.max(this.__margin, pos);
                    pos = Math.min(this.options.basis + this.__margin, pos)
                    this._peak.style["left"] = pos + "px";
                    break;
            }
        } else {
            this._peak.style["display"] = "none";
        }
        this.fire_event("drawpeak", this);
    },
    
    // HELPERS & STUFF
    _bar_size: function () {
        var s = MeterBase.prototype._bar_size.call(this);
        if (this.options.show_clip) {
            var d = (this.options.layout == _TOOLKIT_LEFT
                  || this.options.layout == _TOOLKIT_RIGHT)
                   ? "outer_height" : "outer_width";
            s -= toolkit[d](this._clip, true);
        }
        return s;
    },
    _clip_timeout: function () {
        if (!this.options.auto_clip || this.options.auto_clip < 0) return false;
        if (this.__cto) return;
        if (this.options.clip)
            this.__cto = window.setTimeout(
                this.reset_clip.bind(this),
                this.options.auto_clip);
        else
            this.__cto = null;
    },
    _peak_timeout: function () {
        if (!this.options.auto_peak || this.options.auto_peak < 0) return false;
        if (this.__pto) window.clearTimeout(this.__pto);
        if (this.options.peak > this.options.base
        && this.options.value > this.options.base
        || this.options.peak < this.options.base
        && this.options.value < this.options.base)
            this.__pto = window.setTimeout(
                this.reset_peak.bind(this),
                this.options.auto_peak);
        else
            this.__pto = null;
    },
    _label_timeout: function () {
        var peak_label = (0 | this.options.peak_label);
        var base = +this.options.base;
        var label = +this.options.label;
        var value = +this.options.value;

        if (peak_label <= 0) return false;

        if (this.__lto) return;

        if (label > base && value > base ||
            label < base && value < base)

            this.__lto = window.setTimeout(this.reset_label, peak_label);
        else
            this.__lto = null;
    },
    _top_timeout: function () {
        if (!this.options.auto_hold || this.options.auto_hold < 0) return false;
        if (this.__tto) window.clearTimeout(this.__tto);
        if (this.options.top > this.options.base)
            this.__tto = window.setTimeout(
                this.reset_top.bind(this),
                this.options.auto_hold);
        else
            this.__tto = null;
    },
    _bottom_timeout: function () {
        if (!this.options.auto_hold || this.options.auto_hold < 0) return false;
        if (this.__bto) window.clearTimeout(this.__bto);
        if (this.options.bottom < this.options.base)
            this.__bto = window.setTimeout(
                this.reset_bottom.bind(this),
                this.options.auto_hold);
        else
            this.__bto = null;
    },
    _falling_timeout: function () {
        if (!this.options.falling) return false;
        if (this.__fto) window.clearTimeout(this.__fto);
        if (this._falling > this.options.base
        && this.options.value > this.options.base
        || this._falling < this.options.base
        && this.options.value < this.options.base)
            this.__fto = window.setTimeout(
                this.draw_meter.bind(this),
                1000 / this.options.falling_fps
              * (this.__falling ? 1 : this.options.falling_init));
        else
            this.__fto = null;
    },
    
    // GETTER & SETTER
    set: function (key, value, hold) {
        MeterBase.prototype.set.call(this, key, value, hold);
        switch (key) {
            case "show_peak":
                if (!hold) {
                    this.element.classList[value  ? "add" : "remove"]("toolkit-has-peak");
                    this._peak.style["display"] =  value  ? "block" : "none";
                }
                break;
            case "show_clip":
                if (!hold) {
                    this._clip.style["display"] =  value  ? "block" : "none";
                    this.element.classList[value  ? "add" : "remove"]("toolkit-has-clip");
                    this.redraw();
                }
                break;
            case "show_hold":
                if (!hold) {
                    this.element.classList[value  ? "add" : "remove"]("toolkit-has-hold");
                    this.draw_meter();
                }
                break;
            case "peak":
                if (!hold) {
                    this._peak_timeout();
                    this.draw_peak();
                }
                this.fire_event("peakchanged");
                break;
            case "label":
                if (!hold) {
                    this._label_timeout();
                }
                break;
            case "clip":
                this._clip_timeout();
                if (!hold && value) {
                    this.fire_event("clipping");
                    this.element.classList.add("toolkit-clipping");
                    this.state.set("state", 1);
                } else {
                    this.element.classList.remove("toolkit-clipping");
                    this.state.set("state", 0);
                }
                break;
            case "top":
                this._top_timeout();
                if (!hold) {
                    this.draw_meter();
                }
                this.fire_event("topchanged");
                break;
            case "bottom":
                this._bottom_timeout();
                if (!hold) {
                    this.draw_meter();
                }
                this.fire_event("bottomchanged");
                break;
            case "auto_clip":
                if (this.__cto && !value || value < 0)
                    window.clearTimeout(this.__cto);
                break;
            case "auto_peak":
                if (this.__pto && !value || value < 0)
                    window.clearTimeout(this.__pto);
                break;
            case "peak_label":
                if (this.__lto && !value || value < 0)
                    window.clearTimeout(this.__lto);
                break;
            case "auto_hold":
                if (this.__tto && !value || value < 0)
                    window.clearTimeout(this.__tto);
                if (this.__bto && !value || value < 0)
                    window.clearTimeout(this.__bto);
                break;
        }
    }
});
